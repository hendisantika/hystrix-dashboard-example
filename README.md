# Hystrix Dashboard Example

* http://localhost:8080/rest/hello - URL for triggering Faults or success.
* http://localhost:8080/hystrix - URL for Hystrix Dashboard
* http://localhost:8080/hystrix.stream - URL for Hystrix Stream which can be configured with any Hystrix Dashboard or Turbine

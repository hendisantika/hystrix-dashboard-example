package com.hendisantika.hystrixdashboardexample;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.apache.commons.lang.math.RandomUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by IntelliJ IDEA.
 * Project : hystrix-dashboard-example
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 22/08/18
 * Time: 21.49
 * To change this template use File | Settings | File Templates.
 */

@RestController
@RequestMapping("/rest")
public class HystrixController {
    @HystrixCommand(fallbackMethod = "fallBackHello",
            commandKey = "hello", groupKey = "hello")
    @GetMapping("/hello")
    public String hello() {
        //Wrong
        if (RandomUtils.nextBoolean()) {
            throw new RuntimeException("Failed!");
        }
        return "Hello World";
    }

    @HystrixCommand(fallbackMethod = "fallBackHello",
            commandKey = "helloYT", groupKey = "helloYT")
    @GetMapping("/helloYT")
    public String helloYT() {
        //Wrong
        if (RandomUtils.nextBoolean()) {
            throw new RuntimeException("Failed!");
        }
        return "Hello World Youtube";
    }

    public String fallBackHello() {
        return "Fall Back Hello initiated";
    }
}
